﻿using UnityEngine;

public  class FSMLightswitch  : MonoBehaviour
{
    public LightswitchState currentState;
    public LightswitchState switchedOn;
    public LightswitchState switchedOff;

    private void Awake()
    {
        switchedOn = new SwitchedOn(this);
        switchedOn.FSMLightswitch = this;
        switchedOff = new SwitchedOff(this);
        switchedOff.FSMLightswitch = this;
        currentState = switchedOff;

        
    }

    private void Update()
    {
        currentState.OnUpdate();
    }

    public void ChangeState (LightswitchState state)
    {
        currentState.OnStateExit();
        currentState = state;
        currentState.OnStateEnter();
    }
    
}

public abstract class LightswitchState
{
    public abstract FSMLightswitch FSMLightswitch { get; set; }
    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void OnUpdate();

    public LightswitchState(FSMLightswitch fSMLightswitch)
    {
        FSMLightswitch = fSMLightswitch;
    }


}

public class SwitchedOn : LightswitchState
{
    public override FSMLightswitch FSMLightswitch { get; set; }
    private float timer;
    
    public SwitchedOn(FSMLightswitch fSMLightswitch) : base(fSMLightswitch)
    {
       
    }
    public override void OnStateEnter()
    {
        Debug.Log("Light is on");
    }

    public override void OnStateExit()
    {
        timer = 0f;
    }

    public override void OnUpdate()
    {
        timer += Time.deltaTime;
        if(timer > 5f)
        {
            FSMLightswitch.ChangeState(FSMLightswitch.switchedOff);
        }
    }
}

public class SwitchedOff : LightswitchState
{
    public override FSMLightswitch FSMLightswitch { get; set; }

    public SwitchedOff(FSMLightswitch fSMLightswitch) : base(fSMLightswitch)
    {

    }
    public override void OnStateEnter()
    {
        Debug.Log("Light is Off");
    }

    public override void OnStateExit()
    {

    }

    public override void OnUpdate()
    {
        if (Input.GetButtonDown("Jump"))
        {
            FSMLightswitch.ChangeState(FSMLightswitch.switchedOn);
        }
        
    }
}
