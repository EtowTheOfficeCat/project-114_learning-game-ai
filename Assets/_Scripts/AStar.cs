﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;
using UnityEditor;

public class AStar : MonoBehaviour
{
    [SerializeField] private NavGraph navGraph;
    [SerializeField] private NavNode startNode;
    [SerializeField] private NavNode goalNode;

    private SimplePriorityQueue<int, float> openSet = new SimplePriorityQueue<int, float>();
    private HashSet<int> closedSet = new HashSet<int>();
    private Dictionary<int, int> cameFrom = new Dictionary<int, int>();
    private Dictionary<int, float> gScores = new Dictionary<int, float>();
    private List<int> curPath;


    public List<int> GetPath()
    {
        openSet.Clear();
        closedSet.Clear();
        cameFrom.Clear();
        gScores.Clear();
        curPath.Clear();

        for (int i = 0; i < navGraph.AllNodes.Length; i++)
        {
            gScores.Add(i, Mathf.Infinity);

        }
        // only once for start node:
        float g = 0;
        float h = Vector3.Distance(startNode.transform.position, goalNode.transform.position);
        openSet.Enqueue(startNode.Index, g + h);
        gScores[startNode.Index] = 0f;
        int curNodeIdx = -1;


        while (openSet.Count > 0)
        {
            curNodeIdx = openSet.First;
            if (curNodeIdx == goalNode.Index)
            {
                break;
            }
            curNodeIdx = openSet.Dequeue();
            closedSet.Add(curNodeIdx);
            for (int i = 0; i < navGraph.AllNodes[curNodeIdx].Edges.Count; i++)
            {

                Edge curEdge = navGraph.AllNodes[curNodeIdx].Edges[i];
                int curNeighborIdx = curEdge.ToNode.Index;
                if (closedSet.Contains(curNeighborIdx)) { continue; }
                if (!openSet.Contains(curNeighborIdx))
                {
                    openSet.Enqueue(curNeighborIdx, Mathf.Infinity);
                }
                float tempG = gScores[curNodeIdx] + curEdge.Cost;
                if (tempG > gScores[curNeighborIdx]) { continue; }
                gScores[curNeighborIdx] = tempG;
                if (cameFrom.ContainsKey(curNeighborIdx))
                {
                    cameFrom[curNeighborIdx] = curNodeIdx;
                }
                else
                {
                    cameFrom.Add(curNeighborIdx, curNodeIdx);
                }

                float f = tempG + Vector3.Distance(navGraph.AllNodes[curNeighborIdx].transform.position, goalNode.transform.position);
                openSet.TryUpdatePriority(curNeighborIdx, f);

            }

        }
        List<int> path = new List<int>();
        //path.Add(curNodeIdx);
        while (curNodeIdx != startNode.Index)
        {
            curNodeIdx = cameFrom[curNodeIdx];
            path.Add(curNodeIdx);

        }
        foreach (var item in cameFrom)
        {
            Debug.Log(item.Key + " " + item.Value);
        }
        path.Reverse();
        curPath = path;
        return path;

    }

    private void OnDrawGizmos()
    {
        if (navGraph == null) { return; }
        if (cameFrom.Count < 1) { return; }

        for (int i = 0; i < curPath.Count - 1; i++)
        {
            Vector3 posFrom = navGraph.AllNodes[curPath[i]].transform.position;
            Vector3 posTo = navGraph.AllNodes[curPath[i + 1]].transform.position;

            Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);
            Handles.color = Color.white;
            Handles.ArrowHandleCap(1, posFrom, rot, 4f, EventType.Repaint);
        }

    }
}

[CustomEditor(typeof(AStar))]
public class AStarEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        AStar aStar = (AStar)target;
        if (GUILayout.Button("Lets AState"))
        {
            aStar.GetPath();

        }


    }
}
