﻿using UnityEngine;

public class SimpleGameStates : MonoBehaviour
{
    public Rigidbody rb;
    public SimpleStates currentState;
    public SimpleStates home;
    public SimpleStates mining;
    public SimpleStates bank;
    public SimpleStates saloon;
    public SimpleStates traveling;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        home = new Home(this);
        mining = new Mining(this);
        traveling = new Traveling(this);
        bank = new Bank(this);
        saloon = new Saloon(this);

        currentState = home;
        currentState.OnStateEnter();
    }

    private void Update()
    {
        currentState.OnUpdate();
    }

    private void OnTriggerEnter(Collider other)
    {
        currentState.OnTriggerEnter(other);
        Debug.Log("Collider");
    }

    public void ChangeState(SimpleStates state)
    {
        currentState.OnStateExit();
        currentState = state;
        currentState.OnStateEnter();
    }
}

public abstract class SimpleStates
{
    
    public abstract SimpleGameStates SimpleGameStates { get; set; }
    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void OnUpdate();
    public abstract void OnTriggerEnter(Collider other);

    public SimpleStates nexstate;

    public SimpleStates(SimpleGameStates simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }
}

public class Home : SimpleStates
{
    public override SimpleGameStates SimpleGameStates { get; set; }
    public Home(SimpleGameStates simpleGameStates) : base(simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }

    private float timer;

    public override void OnStateEnter()
    {
        SimpleGameStates.rb.velocity = Vector3.zero;
        Player.goal = GameObject.Find("Mine");
    }

    public override void OnStateExit()
    {
        timer = 0f;
        Traveling.alreadyDrunk = false;
    }

    public override void OnUpdate()
    {
        timer += Time.deltaTime;
        if(timer > 1f)
        {
            SimpleGameStates.ChangeState(SimpleGameStates.traveling);
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
    }
}

public class Mining : SimpleStates
{
    public override SimpleGameStates SimpleGameStates { get; set; }
    public Mining(SimpleGameStates simpleGameStates) : base(simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }

    private float timer;
    public static float Minerals;

    public override void OnStateEnter()
    {
        SimpleGameStates.rb.velocity = Vector3.zero;
        Debug.Log("Mining");
    }

    public override void OnStateExit()
    {
        Traveling.alreadyDrunk = false;
    }

    public override void OnUpdate()
    {
        MiningMinerals();
        Debug.Log(Minerals);
        if (Minerals == 10)
        {
            Player.goal = GameObject.Find("Bank");
            SimpleGameStates.ChangeState(SimpleGameStates.traveling);
        }
        
    }

    public void MiningMinerals()
    {
        timer += Time.deltaTime;
        if(timer > 1f)
        {
            Minerals += 2;
            timer = 0;
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
    }
}

public class Bank : SimpleStates
{
    public override SimpleGameStates SimpleGameStates { get; set; }
    public Bank(SimpleGameStates simpleGameStates) : base(simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }

    private float fullOnMinerals;
    private float timer;

    public override void OnStateEnter()
    {
        SimpleGameStates.rb.velocity = Vector3.zero;
        Debug.Log("Banking");
        fullOnMinerals += Mining.Minerals;
        Mining.Minerals = 0;
    }

    public override void OnStateExit()
    {
        Traveling.alreadyDrunk = false;
        timer = 0;
    }

    public override void OnUpdate()
    {
        timer += Time.deltaTime;

       if(fullOnMinerals == 20)
        {
            if(timer > 2)
            {
            Player.goal = GameObject.Find("Home");
            SimpleGameStates.ChangeState(SimpleGameStates.traveling);
            fullOnMinerals = 0;
            }
        }
       else
        {
            if(timer > 2)
            {
            Player.goal = GameObject.Find("Mine");
            SimpleGameStates.ChangeState(SimpleGameStates.traveling);
            }
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
    }
}

public class Saloon : SimpleStates
{
    public override SimpleGameStates SimpleGameStates { get; set; }
    public Saloon(SimpleGameStates simpleGameStates) : base(simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }

    private float timer;

    public override void OnStateEnter()
    {
        SimpleGameStates.rb.velocity = Vector3.zero;
        Debug.Log("Drinking");
    }

    public override void OnStateExit()
    {
        timer = 0;
    }

    public override void OnUpdate()
    {
        timer += Time.deltaTime;
        if ( timer > 3)
        {
            Player.goal = Traveling.previusGoal;
            SimpleGameStates.ChangeState(SimpleGameStates.traveling);
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
    }
}

public class Traveling : SimpleStates
{
    public override SimpleGameStates SimpleGameStates { get; set; }
    public Traveling(SimpleGameStates simpleGameStates) : base(simpleGameStates)
    {
        SimpleGameStates = simpleGameStates;
    }
    public static GameObject previusGoal;
    public static bool alreadyDrunk = false;
    private float NeedBooz;

    public override void OnStateEnter()
    {
        NeedBooz = Random.Range(-2, 2);
        Debug.Log(NeedBooz);
        if (NeedBooz == 1)
        {
            if (alreadyDrunk) return;
            previusGoal = Player.goal;
            Player.goal = GameObject.Find("Saloon");
            alreadyDrunk = true;
        }

    }

    public override void OnStateExit()
    {
       
    }

    public override void OnUpdate()
    {
        Vector3 diretionToTarget = (Player.goal.transform.position - SimpleGameStates.transform.position).normalized;
        Debug.Log(Player.goal.transform.position.ToString());
        SimpleGameStates.rb.velocity = new Vector3(diretionToTarget.x * Player.Speed, diretionToTarget.y * Player.Speed, diretionToTarget.z * Player.Speed);
        
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Mine"))
        {
            SimpleGameStates.ChangeState(SimpleGameStates.mining);
            Debug.Log("Collider");
        }
        if (other.CompareTag("Bank"))
        {
            SimpleGameStates.ChangeState(SimpleGameStates.bank);
        }

        if (other.CompareTag("Home"))
        {
            SimpleGameStates.ChangeState(SimpleGameStates.home);
        }

        if (other.CompareTag("Saloon"))
        {
            SimpleGameStates.ChangeState(SimpleGameStates.saloon);
        }


    }
}

