﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NavGraphConstructor : MonoBehaviour
{
    [SerializeField] private float handleSize;
    public void OnDrawGizmos()
    {
        Handles.CircleHandleCap(2, transform.position, Quaternion.identity, handleSize, EventType.MouseDown);

    }

    
    
}
