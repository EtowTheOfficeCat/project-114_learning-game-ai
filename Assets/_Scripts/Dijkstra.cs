﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;
using UnityEditor;

public class Dijkstra : MonoBehaviour
{
    [SerializeField] private NavNode startNode;
    [SerializeField] private NavNode endNode;
    List<NavNode> curRoute = new List<NavNode>();
    private NavGraph navGraph;
    private Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();

   public List<NavNode> GetPath()
    {
        navGraph = GetComponent<NavGraph>();
        SimplePriorityQueue<NavNode> frontier = new SimplePriorityQueue<NavNode>();
        frontier.Enqueue(startNode, 0f);
        cameFrom = new Dictionary<NavNode, NavNode>();
        Dictionary<NavNode, float> costSoFar = new Dictionary<NavNode, float>();
        cameFrom.Add(startNode, null);
        costSoFar.Add(startNode, 0f);
        NavNode curNode = null;
        while (frontier.Count > 0)
        {
            curNode = frontier.Dequeue();

            if(curNode == endNode) { break; }
            for (int i = 0; i < curNode.Edges.Count; i++)
            {
                Edge curEdge = curNode.Edges[i];
                float newCost = costSoFar[curNode] + curEdge.Cost;
                if(!costSoFar.ContainsKey(curEdge.ToNode) || newCost < costSoFar[curEdge.ToNode])
                {
                    costSoFar[curEdge.ToNode] = newCost;
                    float priority = newCost;
                    frontier.Enqueue(curEdge.ToNode, priority);
                    cameFrom[curEdge.ToNode] = curNode;
                }
            }
        }
        if(curNode != endNode) { return null; }
        List<NavNode> route = new List<NavNode>();
        while (curNode != startNode)
        {
            route.Add(curNode);
            curNode = cameFrom[curNode];
        }
        route.Add(startNode);
        route.Reverse();
        curRoute = route;
        return route;
    }

    private void OnDrawGizmos()
    {
        if (navGraph == null) { return; }
        if (cameFrom.Count < 1) { return; }

        for (int i = 0; i < curRoute.Count - 1; i++)
        {
            Vector3 posFrom = curRoute[i].transform.position;
            Vector3 posTo = curRoute[i + 1].transform.position;
            Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);
            Handles.color = new Color(1f, 0.7f, 0f);
            Handles.ArrowHandleCap(1, posFrom, rot, 4f, EventType.Repaint);
        }

    }

}

[CustomEditor(typeof(Dijkstra))]
public class DijkstraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Dijkstra dijkstra = (Dijkstra)target;
        if (GUILayout.Button("Lets Dijkstra"))
        {
            dijkstra.GetPath();

        }

        
    }
}
