﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace BT
{
    public class Context
    {

    }

    public class Context<T> : Context
    {
        public T data;
        public Context(T data)
        {
            this.data = data;
        }
    }
        public abstract class Task

        {
            public abstract Status Update(Context context);

            internal int Update()
            {
                throw new NotImplementedException();
            }
        }

        public abstract class Composite : Task
        {
            protected List<Task> children;

            public Composite(List<Task> children)
            {
                this.children = children;
            }
        }

        public abstract class Decorator : Task
        {
            protected Task child;

            public Decorator(Task child)
            {
                this.child = child;
            }
        }

        public interface IMoveable
        {
            void MoveTo(Vector3 targetPos, out bool hasArivved);
        }
        public enum Status
        {
            Succes,
            Failure,
            Running
        };
    
}

