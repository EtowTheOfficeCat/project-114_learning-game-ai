﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class PhysicsJumpTask : Task
    {
        private float force;

        public PhysicsJumpTask(float force)
        {
            this.force = force;
        }
        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            c.data.Debug.text += $"Jump succes\n";
            c.data.RB.AddForce(Vector3.up * force, ForceMode.Impulse);
            return Status.Succes;
        }
    }

}
