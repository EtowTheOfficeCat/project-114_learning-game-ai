﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BT;

public class Agent01 : MonoBehaviour, IMoveable
{
    private Context <AgentData> context;
    private Root root;
    private Status treeStatus = Status.Running;
    [SerializeField] private AgentData data;

    
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform targetTransform;

    public void MoveTo(Vector3 targetPos, out bool HasArrived)
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * moveSpeed);
        HasArrived = (transform.position - targetPos).sqrMagnitude < 0.01f ? true : false;
    }

    void Start()
    {
        context = new Context<AgentData>(data);
        context.data.RB = GetComponent<Rigidbody>();
        context.data.Debug = GetComponentInChildren<TextMesh>();
        context.data.curTargetPos = targetTransform.position;
        context.data.moveable = this;
        var wait = new WaitTask(2f);
        var jump = new PhysicsJumpTask(15f);
        var move = new MoveTask(5f);
        var sequence = new Sequence(new List<Task>() { wait, move });
        var repeater = new Repeater(sequence, 4);
        root = new Root(repeater);
    }

    void Update()
    {
        if(treeStatus != Status.Running)
        {
            return;
        }
        treeStatus = root.Update(context);
    }


}

[System.Serializable]
public class AgentData
{
    public int Level;
    public string Name;
    public Rigidbody RB;
    public TextMesh Debug;
    public Vector3 curTargetPos;
    public IMoveable moveable;
}
