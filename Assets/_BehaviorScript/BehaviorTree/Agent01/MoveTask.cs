﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class MoveTask : Task
    {
        private float maxTime;
        private float timer;

        public MoveTask(float maxTime)
        {
            this.maxTime = maxTime;
        }
        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            c.data.moveable.MoveTo(c.data.curTargetPos, out bool HasArrived);
            if(timer < maxTime)
            {
                return HasArrived ? Status.Succes : Status.Running;
            }
            else
            {
                return Status.Failure;
            }
            
            throw new System.NotImplementedException();
        }
    }
}

