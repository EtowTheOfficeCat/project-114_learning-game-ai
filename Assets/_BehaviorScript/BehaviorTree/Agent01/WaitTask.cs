﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class WaitTask : Task
    {
        private float duration;
        private float timer;

        public WaitTask(float duration)
        {
            this.duration = duration;
        }
        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            timer += Time.deltaTime;
            c.data.Debug.text += $"timer: {timer}\n";
            if (timer >= duration)
            {
                c.data.Debug.text += $"wait done\n";
                timer = 0;
                return Status.Succes;
            }
            else
            {
                c.data.Debug.text += $"wait running\n";
                return Status.Running;
            }
        }
    }
}

