﻿using System.Collections.Generic;

namespace BT
{
    public class Selector : Composite
    {
        private int curChildIdx;

        public Selector(List<Task> children) : base(children)
        {

        }

        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            while (curChildIdx < children.Count)
            {
                switch (children[curChildIdx].Update(context))
                {
                    case Status.Succes:
                        c.data.Debug.text += $"selector succes\n";
                        curChildIdx = 0;
                        return Status.Succes;

                    case Status.Failure:
                        if (++curChildIdx < children.Count)
                        {
                            c.data.Debug.text += $"try next\n";
                            continue;
                        }
                        else
                        {
                            c.data.Debug.text += $"selector fail\n";
                            curChildIdx = 0;
                            return Status.Failure;
                        }
                    case Status.Running:
                        c.data.Debug.text += $"running\n";
                        return Status.Running;
                    default:
                        throw new System.Exception("Sequence : unexpected child status return");

                }
            }
            throw new System.Exception("Sequence : unexpected termination of loop");
        }
    }
}
