﻿using System.Collections.Generic;

namespace BT
{
    public class Sequence : Composite
    {
        private int curChildIdx;

        public Sequence(List<Task> children) : base(children)
        {

        }

        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            while (curChildIdx < children.Count)
            {
                switch (children[curChildIdx].Update(context))
                {
                    case Status.Succes:
                        if (++curChildIdx<children.Count)
                        {
                            c.data.Debug.text += $"sequence continue\n";
                            continue;
                        }
                        else
                        {
                            c.data.Debug.text += $"sequence succes\n";
                            curChildIdx = 0;
                            return Status.Succes;
                        }
                    case Status.Failure:
                        c.data.Debug.text += $"sequence fail\n";
                        curChildIdx = 0;
                        return Status.Failure;
                    case Status.Running:
                        c.data.Debug.text += $"running\n";
                        return Status.Running;
                    default:
                        throw new System.Exception("Sequence : unexpected child status return");

                }
            }
            throw new System.Exception("Sequence : unexpected termination of loop");
        }
    }
}

