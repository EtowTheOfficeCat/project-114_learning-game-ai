﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class Root : Task
    {
        private Task child;
       

        public Root (Task child)
        {
            this.child = child;
            
        }
        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            c.data.Debug.text = "";
            var status = child.Update(context);
            c.data.Debug.text += $"root status {status}";
            return status;
        }
    }
}

