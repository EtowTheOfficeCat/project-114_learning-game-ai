﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class Repeater : Decorator
    {
        private int times;
        private int counter;
        public Repeater (Task child, int times) : base(child)
        {
            this.times = times;
        }
        public override Status Update(Context context)
        {
            Context<AgentData> c = (Context<AgentData>)context;
            while (counter < times)
            {
                switch (child.Update(context))
                {
                    case Status.Succes:
                        if (++counter < times)
                        {
                            c.data.Debug.text += $"repeat child\n";
                            continue;
                        }
                        else
                        {
                            c.data.Debug.text += $"repeated success\n";
                            counter = 0;
                            return Status.Succes;
                        }
                    case Status.Failure:
                        if (++counter < times)
                        {
                            c.data.Debug.text += $"repeat child\n";
                            continue;
                        }
                        else
                        {
                            c.data.Debug.text += $"repeated failure\n";
                            counter = 0;
                            return Status.Failure;
                        }
                        
                    case Status.Running:
                        c.data.Debug.text += $" repeater running\n";
                        return Status.Running;
                    default:
                        throw new System.Exception("Sequence : unexpected child status return");

                }
            }
            throw new System.Exception("Sequence : unexpected termination of loop");
            
            
        }
    }
}

