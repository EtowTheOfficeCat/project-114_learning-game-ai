﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class AlwaysSucced : Decorator
    {
        public AlwaysSucced(Task child) : base(child)
        {

        }
        public override Status Update(Context context)
        {
            switch (child.Update(context))
            {
                case Status.Succes:
                    return Status.Succes;

                case Status.Failure:
                    return Status.Succes;

                case Status.Running:
                    return Status.Running;

                default:
                    throw new System.Exception("unexpected");

            }
        }
    }
}

