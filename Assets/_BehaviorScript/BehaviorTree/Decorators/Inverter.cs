﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    public class Inverter : Decorator
    {
        public Inverter(Task child) : base(child)
        {

        }

        public override Status Update(Context context)
        {
            switch (child.Update(context))
            {
                case Status.Succes:
                    return Status.Failure;

                case Status.Failure:
                    return Status.Failure;

                case Status.Running:
                    return Status.Running;

                default:
                    throw new System.Exception("unexpected");


            }
        }


    }
}

