﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavNode : MonoBehaviour
{
    [SerializeField]private List<Edge> edges = new List<Edge>();
    public List<Edge> Edges => edges;
    public int Index { get; set; }
}

[System.Serializable]
public class Edge
{
    [SerializeField] private NavNode fromNode;
    public NavNode FromNode => fromNode;
    [SerializeField] private NavNode toNode;
    public NavNode ToNode => toNode;

    [SerializeField] private float baseCost;
    public float Cost => baseCost + Distance;
    public float Distance => Vector3.Distance(FromNode.transform.position, ToNode.transform.position);
    public float BaseCost => baseCost;

    public Edge(NavNode fromNode, NavNode toNode)
    {
        this.fromNode = fromNode;
        this.toNode = toNode;
    }
}
