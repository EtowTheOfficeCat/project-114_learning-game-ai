﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NavGraph : MonoBehaviour
{
    [SerializeField] NavGraphVisualizer visualizer = new NavGraphVisualizer();
    private NavNode[] allNodes;
    public NavNode[] AllNodes => allNodes;

    public int GetNodeCount() => transform.childCount;

    public void ConstructGraph()
    {
        int numNodes = transform.childCount;
        allNodes = new NavNode[numNodes];
        Debug.Log("Constructing Graph!");
        for (int i = 0; i < numNodes; i++)
        {
            var navNode = transform.GetChild(i).GetComponent<NavNode>();
            navNode.Index = i;
            allNodes[i] = navNode;
        }
    }
    private void OnDrawGizmos()
    {
        visualizer.DrawGraph(transform);
    }
}

[CustomEditor(typeof(NavGraph))]
public class NavGraphEdit : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        NavGraph navGraph = (NavGraph)target;
        if(GUILayout.Button("Construct NavGraph"))
        {
            navGraph.ConstructGraph();
        }
    }
}

[System.Serializable]
public class NavGraphVisualizer
{
    [SerializeField] private bool visualize = false;
    [SerializeField] private float edgeOffset = 0.1f;
    [SerializeField] private float capSize = 0.03f;
    [SerializeField] private float capOffset = 0.1f;
    [SerializeField] private float costOffset = 0.5f;

    public void DrawGraph(Transform graphTransform)
    {
        if (!visualize) { return; }

        foreach (Transform child in graphTransform)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(child.position, Vector3.one);
            GUIStyle style_Index = new GUIStyle();
            GUIStyle style_cost = new GUIStyle();
            style_Index.fontSize = 28;
            style_cost.fontSize = 12;
            
            var navNode = child.GetComponent<NavNode>();
            Handles.Label(child.position, navNode.Index.ToString(), style_Index);
            Gizmos.color = Color.blue;
            foreach (Edge edge in navNode.Edges)
            {
                if(edge.ToNode == null)
                {
                    continue;
                }
                Vector3 toNodePos = edge.ToNode.transform.position;
                Vector3 dir = (toNodePos - child.position).normalized;
                Vector3 offset = Vector3.Cross(dir, Vector3.up).normalized * edgeOffset;
                Gizmos.DrawLine(child.position + offset, toNodePos + offset);
                Gizmos.DrawWireCube(toNodePos - (dir * capOffset) + offset, Vector3.one * capSize);
                Vector3 costLabelPos = ((child.position + offset * 2) + (toNodePos + offset * 2))* 0.5f + (dir * costOffset);
                Handles.Label(costLabelPos, edge.BaseCost.ToString("0\n") + edge.Cost.ToString("0.0"), style_cost);
            }
        }
        
    }
}
