﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// Todo Visualizierung in der zeit.
public class DFS : MonoBehaviour
{
    [SerializeField] private NavNode startNode;
    [SerializeField] private NavNode endNode;

    private bool[] visited;
    private NavGraph navGraph;

    public List<NavNode> GetPath()
    {
        navGraph = GetComponent<NavGraph>();
        Dictionary<NavNode, NavNode> parentDictionary = new Dictionary<NavNode, NavNode>();
        visited = new bool[navGraph.GetNodeCount()];
        visited[startNode.Index] = true;

        Stack<Edge> edgeStack = new Stack<Edge>();
        for (int i = 0; i < startNode.Edges.Count; i++)
        {
            edgeStack.Push(startNode.Edges[i]);
        }
        while(edgeStack.Count > 0)
        {
            Edge next = edgeStack.Pop();
            if (visited[next.ToNode.Index])
            {
                continue;
            }
            if(next.ToNode.Index == endNode.Index)
            {
                parentDictionary[next.ToNode] = next.FromNode;
                var currentNode = endNode;
                List<NavNode> route = new List<NavNode>();
                while(currentNode != startNode)
                {
                    route.Add(currentNode);
                    currentNode = parentDictionary[currentNode];
                }
                route.Add(startNode);
                route.Reverse();
                foreach (var node in route)
                {
                    Debug.Log(node.Index);
                }
            }
            visited[next.ToNode.Index] = true;
            for (int i = 0; i < next.ToNode.Edges.Count; i++)
            {
                edgeStack.Push(next.ToNode.Edges[i]);
            }
            parentDictionary[next.ToNode] = next.FromNode;
        }
        return null;
    }
}

[CustomEditor(typeof(DFS))]
public class DFSEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        DFS dfs = (DFS)target;
        if (GUILayout.Button("Get Path"))
        {
            dfs.GetPath();
        }

    }
}
