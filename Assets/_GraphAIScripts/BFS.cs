﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BFS : MonoBehaviour
{
    [SerializeField] private NavNode startNode;
    [SerializeField] private NavNode endNode;
    private NavGraph navGraph;
    private Dictionary<int, int> camFrom = new Dictionary<int, int>();
    private List<NavNode> curRoute = new List<NavNode>();

    

    public void ExploreGraph()
    {
        navGraph = GetComponent<NavGraph>();
        Queue<int> frontier = new Queue<int>();
        camFrom.Clear();
        frontier.Enqueue(startNode.Index);
        camFrom.Add(startNode.Index, -1);

        int curNodeIdx = -1;
        while (frontier.Count > 0)
        {
            curNodeIdx = frontier.Dequeue();
            List<Edge> curEdges = navGraph.AllNodes[curNodeIdx].Edges;
            for (int i = 0; i < curEdges.Count; i++)
            {
                if (!camFrom.ContainsKey(curEdges[i].ToNode.Index))
                {
                    frontier.Enqueue(curEdges[i].ToNode.Index);
                    camFrom.Add(curEdges[i].ToNode.Index, curNodeIdx);
                }
            }
            foreach (var keyValuePair in camFrom)
            {
                Debug.Log(keyValuePair.Key + " " + keyValuePair.Value );
            }
        }
    }

    public List<NavNode> GetPath()
    {
        navGraph = GetComponent<NavGraph>();
        Queue<int> frontier = new Queue<int>();
        camFrom.Clear();
        frontier.Enqueue(startNode.Index);
        camFrom.Add(startNode.Index, -1);

        int curNodeIdx = -1;
        while(frontier.Count > 0)
        {
            curNodeIdx = frontier.Dequeue();
            if(curNodeIdx == endNode.Index)
            {
                break;
            }
            List<Edge> curEdges = navGraph.AllNodes[curNodeIdx].Edges;
            for (int i = 0; i < curEdges.Count; i++)
            {
                if (!camFrom.ContainsKey(curEdges[i].ToNode.Index))
                {
                    frontier.Enqueue(curEdges[i].ToNode.Index);
                    camFrom.Add(curEdges[i].ToNode.Index, curNodeIdx);
                }
            }
        }
        if(curNodeIdx != endNode.Index)
        {
            return null;
        }
        List<NavNode> route = new List<NavNode>();
        while ( curNodeIdx != startNode.Index)
        {
            route.Add(navGraph.AllNodes[curNodeIdx]);
            curNodeIdx = camFrom[curNodeIdx];
        }
        route.Add(startNode);
        route.Reverse();
        curRoute = route;

        return route;
    }

    private void OnDrawGizmos()
    {
        if (navGraph == null) { return; }
        if(camFrom.Count < 1) { return; }

        foreach (var keyValuePair in camFrom)
        {
            if (keyValuePair.Value == -1) { continue; }
            Vector3 posFrom = navGraph.AllNodes[keyValuePair.Key].transform.position;
            Vector3 posTo = navGraph.AllNodes[keyValuePair.Value].transform.position;

            Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);
            Handles.color = Color.yellow;
            Handles.ArrowHandleCap(1, posFrom, rot, 4f, EventType.Repaint);
        }
        
        for (int i = 0; i < curRoute.Count - 1; i++)
        {
            Vector3 posFrom = curRoute[i].transform.position;
            Vector3 posTo = curRoute[i + 1].transform.position;
            Quaternion rot = Quaternion.LookRotation((posTo - posFrom).normalized);
            Handles.color = Color.green;
            Handles.ArrowHandleCap(1, posFrom, rot, 4f, EventType.Repaint);
        }

    }
    

}

[CustomEditor(typeof(BFS))]
public class BFSEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        BFS bfs = (BFS)target;
        if(GUILayout.Button("Explore Graph"))
        {
            bfs.ExploreGraph();
           
        }

        if (GUILayout.Button("GetPat"))
        {
            bfs.GetPath();

        }
    }
}
